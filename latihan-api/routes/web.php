<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.create');
});
Route::resource('user', 'UserController');
Route::resource('role', 'RoleController');
// Route::post('/', function () {
//     return view('tampiluser');
// });
// Route::get('/', 'UserController@index');

// Route::get('/insertuser', function () {
//     $user = App\User::create(['nama' => 'Adhi Suwartono']);
//     $all = App\User::all();
//     return [$user,$all];
// });


// Route::get('/tampiluser', function () {
//     $all = App\User::all();
//     return $all;
// });

// Route::get('/tampil', function () {

//     $user = App\User::all();
//     $role = App\Role::all();
//     return [$user,$role];
//     //return view('/tampil', compact(['user','role']));
// });

// Route::get('/insertrole', function () {
//     $user = App\Role::create(['namarole' => 'Administrator']);
//     return $user;
// });


// Route::get('/tampilrole', function () {
//     $all = App\Role::all();
//     return $all;
// });


