<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous"
        />
        <title>User Role</title>
    </head>
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-6">
                    <h2 class="row justify-content-center">Form Input User</h2>

                    <form action="/user" method="post">

                        @csrf
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span
                                    class="input-group-text"
                                    id="inputGroup-sizing-default"
                                    >@ :</span
                                >
                            </div>
                            <input
                                type="text"
                                class="form-control"
                                placeholder="Usernamer"
                                aria-label="Default"
                                aria-describedby="inputGroup-sizing-default"
                                name="username"
                            />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span
                                    class="input-group-text"
                                    id="inputGroup-sizing-default"
                                    >Email :</span
                                >
                            </div>
                            <input
                                type="email"
                                class="form-control"
                                placeholder="Email"
                                aria-label="Default"
                                aria-describedby="inputGroup-sizing-default"
                                name="email"
                            />
                        </div>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span
                                    class="input-group-text"
                                    id="inputGroup-sizing-default"
                                    >Nama Lengkap :</span
                                >
                            </div>
                            <input
                                type="text"
                                class="form-control"
                                placeholder="Nama User"
                                aria-label="Default"
                                aria-describedby="inputGroup-sizing-default"
                                name="name"
                            />
                        </div>

                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label
                                    class="input-group-text"
                                    aria-label="Small"
                                    for="inputGroupSelect01"
                                    >Role :</label
                                >
                            </div>
                            <select
                                class="custom-select"
                                id="inputGroupSelect01" name="role"
                            >
                                <option selected>Pilih Role...</option>
                                <option value="Administrator">
                                    Administrator
                                </option>
                                <option value="User">User</option>
                                <option value="Guest">Guest</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-primary">
                            Tambah User
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>
