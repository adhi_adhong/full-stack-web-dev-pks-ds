<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class Role extends Model
{
  public $timestamps = false;

  protected $fillable = [
    'id', 'name',
  ];

  protected $primaryKey = 'id';
  protected $keyType = 'string';
  public $incrementing = false;

  public function role()
  {
    return $this->hasMany('App\User');
  }

  protected static function boot()
  {
    parent::boot();
    static::creating(function ($model) {
      if (empty($model->{$model->getKeyName()})) {
        $model->{$model->getKeyName()} = Str::uuid();
      }
    });
  }
}
