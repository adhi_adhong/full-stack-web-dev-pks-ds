<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

class UserController extends Controller
{
    public function index()
    {
        $user = User::all();
        return view('user.tampil', compact('user'));

    }

    public function create()
    {
        return view('user.input');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'role' => 'required'
    	]);

        $roleid = Role::select(['id'=> function ($query) {
            $query->where('name', 'role');
        }])->get();

        User::create([
    		'username' => $request->username,
    		'email' => $request->email,
            'name' => $request->name,
            'role_id'=>$roleid
    	]);

    	return redirect('/user');
    }

    public function show($id)
    {
        $user = User::find($id);
        return view('user.show', compact('user'));
    }


    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|unique:user',
            'role' => 'required',
        ]);

        $user = User::find($id);
        $user->name = $request->name;
        $user->role = $request->role;
        $user->update();
        return redirect('/user');
    }


    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/user');
    }

}
