<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function index()
    {
        $user = Role::all();
        return view('role.tampil', compact('role'));

    }

    public function create()
    {
        return view('user.input');
    }

    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'role' => 'required'
    	]);

        //$roleid = App\Role::find($request->role)->user()->where('name', $request->role)->first();

        Role::create([
    		'username' => $request->username,
    		'email' => $request->email,
            'name' => $request->name,
            'role_id'=>$roleid
    	]);

    	return redirect('/user');
    }

    public function show($id)
    {
        $user = Role::find($id);
        return view('user.show', compact('user'));
    }


    public function edit($id)
    {
        $user = Role::find($id);
        return view('user.edit', compact('user'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required|unique:user',
            'role' => 'required',
        ]);

        $user = Role::find($id);
        $user->name = $request->name;
        $user->role = $request->role;
        $user->update();
        return redirect('/user');
    }


    public function destroy($id)
    {
        $user = Role::find($id);
        $user->delete();
        return redirect('/user');
    }
}
