<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class User extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'id', 'username','email','name','role_id'
    ];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    public function user(){
    return $this->belongsTo('App\Role');
    }

    protected static function boot(){
    parent::boot();
    static::creating(function($model){
            if (empty($model->{$model->getKeyName()})){
                $model->{$model->getKeyName()}=Str::uuid();
            }
        });
    }
}
