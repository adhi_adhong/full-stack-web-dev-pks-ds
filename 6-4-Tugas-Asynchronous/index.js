// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini

//Jawaban No 1

console.log("Jawaban No 1.\n")

readBooks(10000, books[0], function (waktusisa) {
    readBooks(waktusisa, books[1], function (waktusisa) {
        readBooks(waktusisa, books[2], function (waktusisa) {
            readBooks(waktusisa, books[3], function (waktusisa) {
                return "selesai baca"
            })
        })
    })
})