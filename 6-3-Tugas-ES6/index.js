// Jawaban No 1

const persegipanjang = (panjang, lebar) => {
    const luas = (panjang1, lebar1) => {
        let p = panjang1;
        let l = lebar1;
        return p * l;
    };

    const keliling = (panjang2, lebar2) => {
        const p = panjang2;
        const l = lebar2;
        return (p + l) * 2;
    };

    console.log("Jawaban No 1");
    console.log("====================");
    console.log("Persegi Panjang :");
    console.log(`Panjang = ${panjang} dan Lebar = ${lebar}`);
    console.log("Maka -->");

    const hasil = `Luas = ${luas(panjang, lebar)}\nKeliling = ${keliling(
        panjang,
        lebar
    )}`;

    return hasil;
};

console.log(persegipanjang(3, 4));
console.log("---------------------");

// Jawaban No 2
const newFunction = (firstName, lastName) => {
    const full = `${firstName} ${lastName}`;
    return {
        fullName: () => {
            console.log(full);
        },
    };
};

console.log("\nJawaban No 2");
console.log("====================");
//Driver Code
newFunction("William", "Imoh").fullName();
console.log("--------------------");

//Jawaban No 3

const newObject = {
    firstName: "Muhammad",
    lastName: "Iqbal Mubarok",
    address: "Jalan Ranamanyar",
    hobby: "playing football",
};

const { firstName, lastName, address, hobby } = newObject;

console.log("\nJawaban No 3");
console.log("====================");
// Driver code
console.log(firstName, lastName, address, hobby);
console.log("--------------------");

//Jawaban No 4
const west = ["Will", "Chris", "Sam", "Holly"];
const east = ["Gill", "Brian", "Noel", "Maggie"];
//const combined = west.concat(east);
const combined = [...west, ...east];

console.log("\nJawaban No 4");
console.log("====================");
//Driver Code
console.log(combined);
console.log("--------------------");

//Jawaban No 5
const planet = "earth";
const view = "glass";
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}`;

console.log("\nJawaban No 5");
console.log("====================");
console.log(before);
console.log("--------------------");
