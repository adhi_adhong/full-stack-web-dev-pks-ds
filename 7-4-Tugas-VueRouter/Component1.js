export const Component1 = {
  data() {
    return {
      components: [
        {
          id: 1,
          title: "Sparepart Mobil (Bensin)",
          description: "ini sparepart mobil mesin bensin",
        },
        {
          id: 2,
          title: "Sparepart Mobil (Diesel)",
          description: "ini sparepart mobil mesin diesel",
        },
      ],
    };
  },
  computed: {
    component() {
      return this.components.filter((component) => {
        return component.id === parseInt(this.$route.params.id);
      })[0];
    },
  },
  template: `<div >
                  <h3>Halaman 1 :  {{ components.title }}</h3>
                  <ul>
                      <li v-for="(num, value) of component">
                          {{ num +' : '+ value }} <br>
                      </li>
                  </ul>
              </div>`,
};
