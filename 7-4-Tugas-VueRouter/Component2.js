export const Component2 = {
  data() {
    return {
      components: [
        {
          id: 1,
          title: "Sparepart Mobil (Bensin)",
          description: "ini sparepart mobil mesin bensin",
        },
        {
          id: 2,
          title: "Sparepart Mobil (Diesel)",
          description: "ini sparepart mobil mesin diesel",
        },
      ],
    };
  },
  computed: {
    category() {
      return this.components.filter((category) => {
        return category.id === parseInt(this.$route.params.id);
      })[0];
    },
  },
  template: `<div >
                  <h3>Halaman 2 :  {{ components.title }}</h3>
                  <ul>
                      <li v-for="(num, value) of category">
                          {{ num +' : '+ value }} <br>
                      </li>
                  </ul>
              </div>`,
};
