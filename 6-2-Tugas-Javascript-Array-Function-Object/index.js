// jawaban no 1
//=====================
    var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"]
    var daftarHewanSort = daftarHewan.sort().slice()
    var text=""
    for (let i = 0; i < daftarHewanSort.length; i++) { 
        text += daftarHewanSort[i] + "\n";
    }
    console.log("\nJawaban No 1. \n")
    console.log(text)

// jawaban no 2
//=====================

    function introduce(datanya) {
        
        return "Nama saya "+ datanya.name+", umur saya "+datanya.age+" tahun, alamat saya di "+datanya.address+", dan saya punya hobby yaitu "+datanya.hobby+"\n"
    } 
    var data = {name : "John" , age : 30 , address : "Jalan Pelesiran" , hobby : "Gaming" }
    
    var perkenalan = introduce(data)
    console.log("Jawaban No 2. \n")
    console.log(perkenalan)

// jawaban no 3
//=====================

    function hitung_huruf_vokal(kata) {
        vokal=0
        for (let i = 0; i < kata.length; i++) {
            if (kata[i].toLowerCase() == "a" || kata[i].toLowerCase() == "i" || kata[i].toLowerCase() == "u" || kata[i].toLowerCase() == "e" || kata[i].toLowerCase() == "o") {
                vokal++
            }
        }
        return vokal
    }

    var hitung_1 = hitung_huruf_vokal("Muhammad")
    var hitung_2 = hitung_huruf_vokal("Iqbal")
    console.log("Jawaban No 3.\n")
    console.log(hitung_1, hitung_2, "\n")
    
// jawaban no 4
//=====================

    function hitung(angka) {
        return angka - 2 + angka
    }
    console.log("Jawaban No 4.\n")
    console.log( hitung(0) ) // -2
    console.log( hitung(1) ) // 0
    console.log( hitung(2) ) // 2
    console.log( hitung(3) ) // 4
    console.log( hitung(5) ) // 8
